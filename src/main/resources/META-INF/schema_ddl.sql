SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS `spring_test` ;

CREATE SCHEMA IF NOT EXISTS `spring_test` DEFAULT CHARACTER SET utf8 ;
USE `spring_test` ;

CREATE TABLE IF NOT EXISTS `spring_test`.`Osoba` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `imie` VARCHAR(25) NOT NULL,
  `nazwisko` VARCHAR(45) NOT NULL,
  `wiek` INT NOT NULL,
  `login` VARCHAR(25) NOT NULL,
  `haslo` VARCHAR(250) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `spring_test`.`Pomiar_Cisnienia` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `skurczowe` INT NOT NULL,
  `rozkurczowe` INT NOT NULL,
  `puls` INT NOT NULL,
  `data_pomiaru` TIMESTAMP NOT NULL,
  `osoba_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_pomiar_cisnienia_osoba_idx` (`osoba_id` ASC),
  CONSTRAINT `fk_pomiar_cisnienia_osoba`
    FOREIGN KEY (`osoba_id`)
    REFERENCES `spring_test`.`Osoba` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `spring_test`.`Rola` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `rola` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `rola_UNIQUE` (`rola` ASC))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `spring_test`.`Osoba_Rola` (
  `osoba_id` INT NOT NULL,
  `rola_id` INT NOT NULL,
  PRIMARY KEY (`osoba_id`, `rola_id`),
  INDEX `fk_osoba_rola_rola_idx` (`rola_id` ASC),
  INDEX `fk_osoba_rola_osoba_idx` (`osoba_id` ASC),
  CONSTRAINT `fk_osoba_rola_osoba`
    FOREIGN KEY (`osoba_id`)
    REFERENCES `spring_test`.`Osoba` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_osoba_rola_rola`
    FOREIGN KEY (`rola_id`)
    REFERENCES `spring_test`.`Rola` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;