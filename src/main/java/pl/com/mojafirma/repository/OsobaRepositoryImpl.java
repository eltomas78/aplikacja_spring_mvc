package pl.com.mojafirma.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import pl.com.mojafirma.model.Osoba;

@Repository
@Transactional
public class OsobaRepositoryImpl implements OsobaRepository {
	
	@PersistenceContext
	private EntityManager em;	

	@Override
	public Osoba getOsobaById(Integer id) {
		return em.find(Osoba.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Osoba> getAllOsoby() {
		return em.createNamedQuery("Osoba.findAll").getResultList();
	}

	@Override
	public Boolean addOsoba(Osoba osoba) {
		if(osoba != null && osoba.getId() == null){
			em.persist(osoba);
			return true;
		}
		return false;
	}

	@Override
	public Boolean editOsoba(Integer id, Osoba osoba) {
		Osoba os = getOsobaById(id);
		if(osoba != null && os != null){			
			os.setImie(osoba.getImie());
			os.setNazwisko(osoba.getNazwisko());
			os.setWiek(osoba.getWiek());
			os.setLogin(osoba.getLogin());
			os.setHaslo(osoba.getHaslo());
			em.persist(os);
			return true;			
		}
		return false;
	}

	@Override
	public Boolean removeOsoba(Integer id) {
		Osoba osoba = getOsobaById(id);
		if(osoba != null){
			em.remove(osoba);
			return true;
		}
		return false;
	}
}
