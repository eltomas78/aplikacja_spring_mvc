package pl.com.mojafirma.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import pl.com.mojafirma.model.PomiarCisnienia;

@Repository
@Transactional
public class PomiarCisnieniaRepositoryImpl implements PomiarCisnieniaRepository {
	
	@PersistenceContext
	private EntityManager em;

	@Override
	public PomiarCisnienia getPomiarCisnieniaById(Integer id) {
		return em.find(PomiarCisnienia.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PomiarCisnienia> getAllPomiarCisnienia() {
		return em.createNamedQuery("PomiarCisnienia.findAll").getResultList();
	}

	@Override
	public Boolean addPomiarCisnienia(PomiarCisnienia pomiarCisnienia) {
		if(pomiarCisnienia != null && pomiarCisnienia.getId() == null){
			em.persist(pomiarCisnienia);
			return true;
		}
		return false;
	}

	@Override
	public Boolean editPomiarCisnienia(Integer id, PomiarCisnienia pomiarCisnienia) {
		PomiarCisnienia pomCis = getPomiarCisnieniaById(id);
		if(pomiarCisnienia != null && pomCis != null){
			pomCis.setDataPomiaru(pomiarCisnienia.getDataPomiaru());
			pomCis.setOsoba(pomiarCisnienia.getOsoba());
			pomCis.setPuls(pomiarCisnienia.getPuls());
			pomCis.setRozkurczowe(pomiarCisnienia.getRozkurczowe());
			pomCis.setSkurczowe(pomiarCisnienia.getRozkurczowe());
			em.merge(pomCis);
			return true;
		}
		return false;
	}

	@Override
	public Boolean removePomiarCisnienia(Integer id) {
		PomiarCisnienia pomiarCisnienia = getPomiarCisnieniaById(id);
		if(pomiarCisnienia != null){
			em.remove(pomiarCisnienia);
			return true;
		}
		return false;
	}

}
