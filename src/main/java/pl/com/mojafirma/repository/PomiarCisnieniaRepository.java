package pl.com.mojafirma.repository;

import java.util.List;

import pl.com.mojafirma.model.PomiarCisnienia;

public interface PomiarCisnieniaRepository {
	
	PomiarCisnienia getPomiarCisnieniaById(Integer id);
	
	List<PomiarCisnienia> getAllPomiarCisnienia();
	
	Boolean addPomiarCisnienia(PomiarCisnienia pomiarCisnienia);
	Boolean editPomiarCisnienia(Integer id, PomiarCisnienia pomiarCisnienia);
	Boolean removePomiarCisnienia(Integer id);
}
