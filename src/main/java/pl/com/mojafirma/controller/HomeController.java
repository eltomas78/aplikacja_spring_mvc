package pl.com.mojafirma.controller;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import pl.com.mojafirma.model.Osoba;
import pl.com.mojafirma.service.OsobaService;

@Controller
public class HomeController {
	
	@Autowired
	private OsobaService osobaService;
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);	

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model, HttpServletRequest request) {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		
		Osoba osoba = osobaService.getOsobaById(1);
		List<Osoba> osoby = osobaService.getAllOsoby();
		
		model.addAttribute("serverTime", formattedDate );
		model.addAttribute("osoba", osoba );
		model.addAttribute("osoby", osoby );
		
		return "home";
	}	
}
