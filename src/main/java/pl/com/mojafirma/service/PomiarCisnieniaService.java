package pl.com.mojafirma.service;

import java.util.List;

import pl.com.mojafirma.model.PomiarCisnienia;

public interface PomiarCisnieniaService {
	
	PomiarCisnienia getPomiarCisnieniaById(Integer id);
	
	List<PomiarCisnienia> getAllPomiarCisnienia();
	
	Boolean addPomiarCisnienia(PomiarCisnienia pomiarCisnienia);
	Boolean editPomiarCisnienia(Integer id, PomiarCisnienia pomiarCisnienia);
	Boolean removePomiarCisnienia(Integer id);
}
