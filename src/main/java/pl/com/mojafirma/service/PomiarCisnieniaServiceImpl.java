package pl.com.mojafirma.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pl.com.mojafirma.model.PomiarCisnienia;
import pl.com.mojafirma.repository.PomiarCisnieniaRepository;

@Service
@Transactional
public class PomiarCisnieniaServiceImpl implements PomiarCisnieniaService {
	
	@Autowired
	private PomiarCisnieniaRepository pomiarCisnieniaRepository;

	@Override
	public PomiarCisnienia getPomiarCisnieniaById(Integer id) {
		return pomiarCisnieniaRepository.getPomiarCisnieniaById(id);
	}

	@Override
	public List<PomiarCisnienia> getAllPomiarCisnienia() {
		return pomiarCisnieniaRepository.getAllPomiarCisnienia();
	}

	@Override
	public Boolean addPomiarCisnienia(PomiarCisnienia pomiarCisnienia) {
		return pomiarCisnieniaRepository.addPomiarCisnienia(pomiarCisnienia);
	}

	@Override
	public Boolean editPomiarCisnienia(Integer id, PomiarCisnienia pomiarCisnienia) {
		return pomiarCisnieniaRepository.editPomiarCisnienia(id, pomiarCisnienia);
	}

	@Override
	public Boolean removePomiarCisnienia(Integer id) {
		return pomiarCisnieniaRepository.removePomiarCisnienia(id);
	}
}
