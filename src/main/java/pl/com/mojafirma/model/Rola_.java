package pl.com.mojafirma.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-10-16T16:38:54.762+0200")
@StaticMetamodel(Rola.class)
public class Rola_ {
	public static volatile SingularAttribute<Rola, Integer> id;
	public static volatile SingularAttribute<Rola, String> rola;
	public static volatile SetAttribute<Rola, Osoba> osoby;
}
