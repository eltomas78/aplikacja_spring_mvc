package pl.com.mojafirma.model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.sql.Timestamp;

@Entity
@Table(name="pomiar_cisnienia")
@NamedQuery(name="PomiarCisnienia.findAll", query="SELECT p FROM PomiarCisnienia p")
public class PomiarCisnienia implements Serializable {

	private static final long serialVersionUID = -7836677130810497451L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private Integer id;

	@Column(name="data_pomiaru", nullable=false)
	private Timestamp dataPomiaru;

	@Column(nullable=false)
	private Integer puls;

	@Column(nullable=false)
	private Integer rozkurczowe;

	@Column(nullable=false)
	private Integer skurczowe;

	//bi-directional many-to-one association to Osoba
	@ManyToOne
	@JoinColumn(name="osoba_id", nullable=false)	
	@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
	private Osoba osoba;

	public PomiarCisnienia() {
	}
	
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Timestamp getDataPomiaru() {
		return this.dataPomiaru;
	}

	public void setDataPomiaru(Timestamp dataPomiaru) {
		this.dataPomiaru = dataPomiaru;
	}

	public Integer getPuls() {
		return this.puls;
	}

	public void setPuls(Integer puls) {
		this.puls = puls;
	}

	public Integer getRozkurczowe() {
		return this.rozkurczowe;
	}

	public void setRozkurczowe(Integer rozkurczowe) {
		this.rozkurczowe = rozkurczowe;
	}

	public Integer getSkurczowe() {
		return this.skurczowe;
	}

	public void setSkurczowe(Integer skurczowe) {
		this.skurczowe = skurczowe;
	}

	public Osoba getOsoba() {
		return this.osoba;
	}

	public void setOsoba(Osoba osoba) {
		this.osoba = osoba;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dataPomiaru == null) ? 0 : dataPomiaru.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((puls == null) ? 0 : puls.hashCode());
		result = prime * result + ((rozkurczowe == null) ? 0 : rozkurczowe.hashCode());
		result = prime * result + ((skurczowe == null) ? 0 : skurczowe.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof PomiarCisnienia)) {
			return false;
		}
		PomiarCisnienia other = (PomiarCisnienia) obj;
		if (dataPomiaru == null) {
			if (other.dataPomiaru != null) {
				return false;
			}
		} else if (!dataPomiaru.equals(other.dataPomiaru)) {
			return false;
		}
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (puls == null) {
			if (other.puls != null) {
				return false;
			}
		} else if (!puls.equals(other.puls)) {
			return false;
		}
		if (rozkurczowe == null) {
			if (other.rozkurczowe != null) {
				return false;
			}
		} else if (!rozkurczowe.equals(other.rozkurczowe)) {
			return false;
		}
		if (skurczowe == null) {
			if (other.skurczowe != null) {
				return false;
			}
		} else if (!skurczowe.equals(other.skurczowe)) {
			return false;
		}
		return true;
	}
}