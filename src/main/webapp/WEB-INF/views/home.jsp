<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false" language="java"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>Home</title>
</head>
<body>
	<h1>Hello world!</h1>

	<p>The time on the server is ${serverTime}.</p>
	<p>osoba: ${osoba.imie}, ${osoba.nazwisko}</p>
	<ul>
		<c:forEach items="${osoby}" var="os">
			<li>id: ${os.id}; imie: ${os.imie}; nazwisko: ${os.nazwisko}</li>
		</c:forEach>
	</ul>

</body>
</html>
