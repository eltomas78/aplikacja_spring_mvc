USE `spring_test`;

LOCK TABLES `osoba` WRITE;
INSERT INTO `osoba` VALUES (1,'Jan','Kowalski',33,'jkowalski','kowalskihaslo'),(2,'Anna','Nowak',25,'anowak','nowakhaslo');
UNLOCK TABLES;

LOCK TABLES `pomiar_cisnienia` WRITE;
INSERT INTO `pomiar_cisnienia` VALUES (2,125,80,60,'2016-09-20 08:05:00',2),(3,130,82,64,'2016-09-20 18:22:00',2);
UNLOCK TABLES;

LOCK TABLES `rola` WRITE;
INSERT INTO `rola` VALUES (1,'ROLE_ADMIN'),(2,'ROLE_USER');
UNLOCK TABLES;

LOCK TABLES `osoba_rola` WRITE;
INSERT INTO `osoba_rola` VALUES (1,1),(1,2),(2,2);
UNLOCK TABLES;